import { demos } from './data';

const initialState = {
  items: demos,
} 

export default initialState;
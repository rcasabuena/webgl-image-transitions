import React from 'react';
import { hot } from 'react-hot-loader';
import { Router } from "@reach/router";
import './App.css';
import Frame from './components/Frame';
import Demo from './components/Demo';
import Cursor from './components/Cursor';

const App = () => (
  <main className="demo-4">
      <Frame />
      <Router>
        <Demo path="/" default />
        <Demo path="/:demo" />
      </Router>
      <Cursor />
  </main>
);

export default hot(module)(App);
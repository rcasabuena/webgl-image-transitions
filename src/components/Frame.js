import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from "@reach/router";
import { getItems } from './Utils'

class Frame extends Component {
  render() {
    const { items } = this.props;

    return (
      <div className="frame">
        <div className="frame__title-wrap">
          <h1 className="frame__title"><Link to="/">WebGL Image Transitions</Link></h1>
        </div>
        <div className="frame__links">
          <a target="_blank" href="https://bitbucket.org/rcasabuena/webgl-image-transitions">Bitbucket</a>
        </div>
        <div className="frame__info">Click the image</div>
        <div className="frame__demos">
          {items.map(item => <Link key={item.name} getProps={({isCurrent, location}) => {
            return ({className: isCurrent || (location.pathname == "/" && item.name == 'demo1') ? "frame__demo frame__demo--current" : "frame__demo"});
          }} to={item.name}>{item.linkName}</Link>)}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    items: getItems(state),
  }
}

export default connect(mapStateToProps)(Frame);
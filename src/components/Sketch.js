import * as THREE from 'three';
import * as dat from 'dat.gui';
import { TimelineMax, Power2 } from 'gsap';

export const Sketch = options => () => {
  let scene = new THREE.Scene();
  let vertex = `varying vec2 vUv;void main() {vUv = uv;gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );}`;
  let fragment = options.fragment;
  let uniforms = options.uniforms;
  let width = window.innerWidth;
  let height = window.innerHeight;

  let renderer = new THREE.WebGLRenderer();
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(width, height);
  renderer.setClearColor(0xeeeeee, 1);
  
  let duration = options.duration || 1;
  let debug = options.debug || false;
  let easing = options.easing || 'easeInOut';

  let clicker = document.getElementById("content");
  let container = document.getElementById("slider");

  const clearCanvas = () => {
    container.innerHTML = "";
  }
  clearCanvas();

  let images = options.images;
  width = container.offsetWidth;
  height = container.offsetHeight;
  container.appendChild(renderer.domElement);
  let camera = new THREE.PerspectiveCamera(
    70,
    window.innerWidth / window.innerHeight,
    0.001,
    1000
  );

  camera.position.set(0, 0, 2);
  let time = 0;
  let current = 0;
  let textures = [];

  let paused = true;
  let gui, material, plane, imageAspect, geometry, isRunning;

  const initiate = cb => {
    loading();
    let texturesp = textures;
    const promises = [];
    images.forEach((url,i)=>{
      let promise = new Promise(resolve => {
        texturesp[i] = new THREE.TextureLoader().load( url, resolve );
      });
      promises.push(promise);
    })

    Promise.all(promises).then(() => {
      cb();
      loaded();
    });
  }

  const loading = () => {
    document.body.classList.add("loading");
    document.body.classList.remove("loaded");
  }

  const loaded = () => {
    document.body.classList.remove("loading");
    document.body.classList.add("loaded");
  }

  const clickEvent = () => {
    clicker.addEventListener('click',()=>{
      next();
    });
  };

  let settings = () => {
    if(debug) {
      gui = new dat.GUI();
    }
    settings = { progress: 0.5 };

    Object.keys(uniforms).forEach((item)=> {
      settings[item] = uniforms[item].value;
      if(debug) gui.add(settings, item, uniforms[item].min, uniforms[item].max, 0.01);
    });
  };

  const setupResize = () => {
    window.addEventListener("resize", resize);
  };

  const resize = () => {
    width = container.offsetWidth;
    height = container.offsetHeight;
    renderer.setSize(width, height);
    camera.aspect = width / height;
    

    // image cover
    imageAspect = textures[0].image.height/textures[0].image.width;
    let a1; let a2;
    if(height/width>imageAspect) {
      a1 = (width/height) * imageAspect ;
      a2 = 1;
    } else{
      a1 = 1;
      a2 = (height/width) / imageAspect;
    }

    material.uniforms.resolution.value.x = width;
    material.uniforms.resolution.value.y = height;
    material.uniforms.resolution.value.z = a1;
    material.uniforms.resolution.value.w = a2;

    const camdist  = camera.position.z;
    const camheight = 1;
    camera.fov = 2*(180/Math.PI)*Math.atan(camheight/(2*camdist));

    plane.scale.x = camera.aspect;
    plane.scale.y = 1;

    camera.updateProjectionMatrix();
  };

  const addObjects = () => {
    material = new THREE.ShaderMaterial({
      extensions: {
        derivatives: "#extension GL_OES_standard_derivatives : enable"
      },
      side: THREE.DoubleSide,
      uniforms: {
        time: { type: "f", value: 0 },
        progress: { type: "f", value: 0 },
        border: { type: "f", value: 0 },
        intensity: { type: "f", value: 0 },
        scaleX: { type: "f", value: 40 },
        scaleY: { type: "f", value: 40 },
        transition: { type: "f", value: 40 },
        swipe: { type: "f", value: 0 },
        width: { type: "f", value: 0 },
        radius: { type: "f", value: 0 },
        texture1: { type: "f", value: textures[0] },
        texture2: { type: "f", value: textures[1] },
        displacement: { type: "f", value: new THREE.TextureLoader().load('img/disp1.jpg') },
        resolution: { type: "v4", value: new THREE.Vector4() },
      },
      // wireframe: true,
      vertexShader: vertex,
      fragmentShader: fragment
    });

    geometry = new THREE.PlaneGeometry(1, 1, 2, 2);
    plane = new THREE.Mesh(geometry, material);
    scene.add(plane);
  };

  const stop = () => {
    paused = true;
  };

  const play = () => {
    paused = false;
    render();
  };

  const next = () => {
    if(isRunning) return;
    isRunning = true;
    let len = textures.length;
    let nextTexture = textures[(current +1)%len];
    material.uniforms.texture2.value = nextTexture;
    let tl = new TimelineMax();
    tl.to(material.uniforms.progress, duration, {
      value: 1,
      ease: Power2[easing],
      onComplete: () => {
        console.log('FINISH');
        current = (current +1)%len;
        material.uniforms.texture1.value = nextTexture;
        material.uniforms.progress.value = 0;
        isRunning = false;
      }
    });
  };
  
  const render = () => {
    if (paused) return;
    time += 0.05;
    material.uniforms.time.value = time;

    Object.keys(uniforms).forEach((item)=> {
      material.uniforms[item].value = settings[item];
    });

    requestAnimationFrame(render);
    renderer.render(scene, camera);
  };

  return {
    build: () => initiate(() => {
      setupResize();
      settings();
      addObjects();
      resize();
      clickEvent();
      play();
    })
  }
}
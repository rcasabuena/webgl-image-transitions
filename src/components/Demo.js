import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getOptions, updateMeta, getItems, setBodyClass } from './Utils'
import { Sketch } from './Sketch';

class Demo extends Component {

  componentDidUpdate() {
    this.handleChange();
  }

  componentDidMount() {
    this.handleChange();
  }

  handleChange() {
    const { options, items } = this.props;
    const demo = Sketch(options)();
    demo.build();
    updateMeta(options);
    setBodyClass(options, items);
  }

  render() {
    return (
      <div id="content" className="content" >
        <div id="slider"></div>
      </div>
    );
  }
};

const mapStateToProps = (state, props) => {
  return {
    options: getOptions(state, props),
    items: getItems(state)
  }
}

export default connect(mapStateToProps)(Demo);
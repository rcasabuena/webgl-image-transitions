export const MathUtils = {
  lineEq: (y2, y1, x2, x1, currentVal) => {
      // y = mx + b 
      var m = (y2 - y1) / (x2 - x1), b = y1 - m * x1;
      return m * currentVal + b;
  },
  lerp: (a, b, n) => (1 - n) * a + n * b,
  getRandomFloat: (min, max) => (Math.random() * (max - min) + min).toFixed(2)
};

export const getMousePos = (e) => {
  let posx = 0;
  let posy = 0;
  if (!e) e = window.event;
  if (e.pageX || e.pageY) {
      posx = e.pageX;
      posy = e.pageY;
  }
  else if (e.clientX || e.clientY)    {
      posx = e.clientX + body.scrollLeft + document.documentElement.scrollLeft;
      posy = e.clientY + body.scrollTop + document.documentElement.scrollTop;
  }
  return { x : posx, y : posy }
};

export const calcWinsize = () => ({width: window.innerWidth, height: window.innerHeight});

export const getOptions = ({items}, props) => {
  return props.default ? items[0] : items.find(item => item.name === props.demo);
}

export const getItems = ({items}) => {
  return items;
}

export const updateMeta = (options) => {
  document.title = options.title;
}

export const setBodyClass = (options, items) => {
  const docBody = document.body;
  items.map(item => docBody.classList.remove(item.name));
  docBody.classList.add(options.name);
}
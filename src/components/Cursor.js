import React, { Component } from 'react';
import { getMousePos, MathUtils, calcWinsize } from './Utils';

class Cursor extends Component {

  renderCursor = () => {
    this.winsize = calcWinsize();
    window.addEventListener('resize', () => this.winsize = calcWinsize());
    this.mousepos = {x: this.winsize.width/2, y: this.winsize.height/2};
    window.addEventListener('mousemove', ev => this.mousepos = getMousePos(ev));

    [...document.querySelectorAll('a')].forEach((link) => {
      link.addEventListener('mouseenter', () => this.enter());
      link.addEventListener('mouseleave', () => this.leave());
    });

    this.bounds = this.circle.getBoundingClientRect();
    this.renderedStyles = {
      tx: {previous: 0, current: 0, amt: 0.2},
      ty: {previous: 0, current: 0, amt: 0.2},
      scale: {previous: 1, current: 1, amt: 0.2}
    };
    requestAnimationFrame(() => this.renderer());
  }

  renderer() {
    this.renderedStyles['tx'].current = this.mousepos.x - this.bounds.width/2;
    this.renderedStyles['ty'].current = this.mousepos.y - this.bounds.height/2;

    for (const key in this.renderedStyles ) {
        this.renderedStyles[key].previous = MathUtils.lerp(this.renderedStyles[key].previous, this.renderedStyles[key].current, this.renderedStyles[key].amt);
    }
                
    this.circle.style.transform = `translateX(${(this.renderedStyles['tx'].previous)}px) translateY(${this.renderedStyles['ty'].previous}px) scale(${this.renderedStyles['scale'].previous})`;
    requestAnimationFrame(() => this.renderer());
  }

  enter() {
    this.renderedStyles['scale'].current = 1.9;
  }
  leave() {
    this.renderedStyles['scale'].current = 1;
  }

  componentDidMount() {
    this.renderCursor();
  }

  render() {
    return (
      <div ref={el => this.cursor = el} className="cursor">
        <div ref={el => this.circle = el} className="cursor__inner cursor__inner--circle"></div>
      </div>
    )
  }

}

export default Cursor;